import React from 'react'
import { NavLink } from 'react-router-dom'
import DATA from '../Data'

const Product = () => {

    const cardItem = (item) => {
        return (
            <>
                <div className="col">
                    <div className="card h-100" key={item.id} >
                        <img src={item.img} className="card-img-top" alt={item.title} height={250} />
                        <div className="card-body">
                            <h5 className="card-title">{item.title}</h5>
                            <p className="card-text">
                                ${item.price}
                            </p>
                        </div>
                        <div className="card-footer d-flex justify-content-center">
                            <small className="text-muted">
                                <NavLink to={`/products/${item.id}`} className="btn btn-outline-dark">View</NavLink>
                            </small>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    return (
        <div>
            <div className="container py-5">
                <div className="row">
                    <div className="col-12 text-center">
                        <h1 className="display-6 fw-bolder text-center">Product</h1>
                        <hr />
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row row-cols-1 row-cols-md-4 g-4">
                    {DATA.map(cardItem)}
                </div>
            </div>
        </div>
    )
}

export default Product