import React from 'react'
import { NavLink } from 'react-router-dom'

const Footer = () => {
    return (
        <div>
            <div className="container-fluid">
                <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
                    <div className="col-md-4 d-flex align-items-center">
                        <NavLink to="/" className="mb-3 me-2 mb-md-0 text-body-secondary text-decoration-none lh-1">
                            <svg className="bi" width={30} height={24}><use to="#bootstrap" /></svg>
                        </NavLink>
                        <span className="mb-3 mb-md-0 text-body-secondary">&copy; 2023 Company, Inc</span>
                    </div>

                    <ul className="nav col-md-4 justify-content-end list-unstyled d-flex">
                        <li className="ms-3"><NavLink className="text-body-secondary" to="#"><svg className="bi" width={24} height={24}><use to="#twitter" /></svg></NavLink></li>
                        <li className="ms-3"><NavLink className="text-body-secondary" to="#"><svg className="bi" width={24} height={24}><use to="#instagram" /></svg></NavLink></li>
                        <li className="ms-3"><NavLink className="text-body-secondary" to="#"><svg className="bi" width={24} height={24}><use to="#facebook" /></svg></NavLink></li>
                    </ul>
                </footer>
            </div>
        </div>
    )
}

export default Footer