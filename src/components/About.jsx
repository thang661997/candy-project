import React from 'react'
import { NavLink } from 'react-router-dom'

const About = () => {
    return (
        <div>
            <div className="container py-5 my-5">
                <div className="row">
                    <div className="col-md-6">
                        <h1 className="text-dark fw-bold mb-4">About Us</h1>
                        <p className="lead mb-4">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum magni officiis ab aspernatur nesciunt reiciendis impedit quod eaque quos. Voluptatum velit fuga maxime iste? Velit error dignissimos similique quo.
                        </p>
                        <NavLink to="/contact" className="btn btn-outline-dark px-3">Contact Us</NavLink>
                    </div>
                    <div className="col-md-6 d-flex justify-content-center">
                        <img src="/assets/images/about.png" alt="About Us" height={400} width={400} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About